package ru.t1.gorodtsova.tm.api.service.model;

import ru.t1.gorodtsova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>, IUserOwnedRepository<M> {
}
