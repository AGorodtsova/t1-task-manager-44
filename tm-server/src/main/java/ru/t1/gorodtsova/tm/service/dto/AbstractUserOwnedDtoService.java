package ru.t1.gorodtsova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.gorodtsova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedDtoService
        <M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R>
        implements IUserOwnedDtoService<M> {

    public AbstractUserOwnedDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected abstract IUserOwnedDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model.setUserId(userId);
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            @Nullable final M model = repository.findOneById(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (!userId.equals(model.getUserId())) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            findOneById(userId, id);
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            return repository.findOneById(userId, id) != null;
        } finally {
            entityManager.close();
        }
    }

}
