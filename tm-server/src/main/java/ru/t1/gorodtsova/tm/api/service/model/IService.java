package ru.t1.gorodtsova.tm.api.service.model;

import ru.t1.gorodtsova.tm.api.repository.model.IRepository;
import ru.t1.gorodtsova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
