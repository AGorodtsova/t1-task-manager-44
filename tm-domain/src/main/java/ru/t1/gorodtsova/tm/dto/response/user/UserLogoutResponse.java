package ru.t1.gorodtsova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.gorodtsova.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResultResponse {
}
