package ru.t1.gorodtsova.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
